# Sử dụng image cơ sở chứa PHP và Apache
FROM php:7.4-apache

# Cài đặt các extension PHP cần thiết
RUN docker-php-ext-install mysqli pdo pdo_mysql

# Sao chép mã nguồn ứng dụng web vào thư mục gốci của Apache
COPY * /var/www/html

# Thiết lập lệnh mặc định khi container được chạy
CMD ["apache2-foreground"]
